
install_composer:
	docker build -t webhookserver:run-0.0.1 -f web-hook-server/docker/Dockerfile.run web-hook-server
	cd web-hook-server && docker run --name web-hook-server_2 --rm -v "${PWD}/web-hook-server:/usr/src/app" -w /usr/src/app webhookserver:run-0.0.1 composer install
	cd web-hook-server && docker run --name web-hook-server_2 --rm -v "${PWD}/web-hook-server:/usr/src/app" -w /usr/src/app webhookserver:run-0.0.1 php artisan key:generate

webhook_build:
	docker build -t webhookserver:0.0.1 -f web-hook-server/docker/Dockerfile web-hook-server

webhook_start:
	docker-compose up -d
	sudo chown -R 998:998 web-hook-server/storage

webhook_stop:
	docker-compose down
	sudo chown -R ${USER}:${USER} web-hook-server/storage


