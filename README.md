# Facebook Webhook with Laravel


### Set Environment Local

```bash
export WWWGROUP=1000
export WWWGROUP_ID=998
export WWWUSER=sail
export ENVIRONMENT=local

```

### Create Docker image

```bash
make webhook_build
```

### Install Laravel

```bash
make install_composer

```

### Start Webhook Service

```bash
make webhook_start

```

### Start Webhook Service

```bash
make webhook_stop

```

